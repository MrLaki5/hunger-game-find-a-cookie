// Milan Lazarevic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/TriggerVolume.h"
#include "GameFramework/Actor.h"
#include "OpenDoor4.generated.h"

//Declares delegate used for broadcasting event for opening and closing last door
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent4);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEGAME_API UOpenDoor4 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor4();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Open door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent4 OnOpenRequest;

	//Close door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent4 OnCloseRequest;

private:

	//Table1 preassure plate
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate1;

	//Table2 preassure plate
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate2;

	//Table3 preassure plate
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate3;
	
	//Table lamp1
	UPROPERTY(EditAnywhere)
		AActor* item1;

	//Table lamp2
	UPROPERTY(EditAnywhere)
		AActor* item2;

	//Table lamp3
	UPROPERTY(EditAnywhere)
		AActor* item3;
	
};
