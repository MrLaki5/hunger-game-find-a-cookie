// Milan Lazarevic

#include "OpenDoor4.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UOpenDoor4::UOpenDoor4()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor4::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UOpenDoor4::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bool openF = false;
	//Sets flag true when all table laps (items) are on tables (preassure plates)
	if (PressurePlate1->IsOverlappingActor(item1) || PressurePlate1->IsOverlappingActor(item2) || PressurePlate1->IsOverlappingActor(item3)) {
		if (PressurePlate2->IsOverlappingActor(item1) || PressurePlate2->IsOverlappingActor(item2) || PressurePlate2->IsOverlappingActor(item3)) {
			if (PressurePlate3->IsOverlappingActor(item1) || PressurePlate3->IsOverlappingActor(item2) || PressurePlate3->IsOverlappingActor(item3)) {
				openF = true;
			}
		}
	}
	//Send signal to open last door
	if (openF) {
		OnOpenRequest.Broadcast();
	}
	else {
		OnCloseRequest.Broadcast();
	}
}

