// Milan Lazarevic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Grabber.generated.h"

//Declares delegate used for broadcasting event for ending game on cookie clicked
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEndGameEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEGAME_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//End game event
	UPROPERTY(BlueprintAssignable)
		FEndGameEvent OnGameEnd;

private:
	//Multiplier for where grabed object will be drawn
	float Reach;

	//Handle for grabing and releasing object
	UPhysicsHandleComponent* PhysicsHandle= nullptr;

	//Handle for player keyboard events
	UInputComponent* InputHandle = nullptr;

	//Method for mapped key events
	//On release key event
	void Release();
	//On press key event
	void Grab();
	//On press esc
	void Quit();
};
