// Milan Lazarevic

#include "Grabber.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include <string>
#include "Runtime/Engine/Public/DrawDebugHelpers.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

//Called on pressed button
void UGrabber::Grab() {
	FRotator tempRot;
	FVector tempVec;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(tempVec, tempRot);
	FVector TempLine = tempVec + (tempRot.Vector()*Reach);

	//Debug line drawing
	//DrawDebugLine(GetWorld(), tempVec, TempLine, FColor(255, 0, 0), false, 0.0f, 0.0f, 10.0f);

	//Line trace, check if some actor with physical body is hit
	FHitResult Hit;
	if (GetWorld()->LineTraceSingleByObjectType(Hit, tempVec, TempLine, FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), FCollisionQueryParams(FName(TEXT("")), false, GetOwner()))) {
		//UE_LOG(LogTemp, Warning, TEXT("Hit object %s"), *Hit.Actor->GetName());

		//Grab object
		PhysicsHandle->GrabComponent(Hit.GetComponent(), NAME_None, Hit.GetActor()->GetActorLocation(), true);
	}
	else {
		//Check if object is Cookie, if it is broadcast eng game event
		if (GetWorld()->LineTraceSingleByObjectType(Hit, tempVec, TempLine, FCollisionObjectQueryParams(ECollisionChannel::ECC_WorldStatic), FCollisionQueryParams(FName(TEXT("")), false, GetOwner()))) {
			std::string TestString = "_";
			std::string TestString2 = "Cookie";
			FString splitString(TestString.c_str());
			FString cookieString(TestString2.c_str());
			FString rightStr;
			FString leftStr;
			Hit.Actor->GetName().Split(splitString, &leftStr, &rightStr, ESearchCase::IgnoreCase, ESearchDir::FromStart);
			if (leftStr.Compare(cookieString, ESearchCase::IgnoreCase)==0) {
				OnGameEnd.Broadcast();
			}
		}
	}
}

//Called on released pressed button
void UGrabber::Release() {
	//Release grabed object
	PhysicsHandle->ReleaseComponent();
}

//Called on esc pressed button
void UGrabber::Quit() {
	//quit game
	OnGameEnd.Broadcast();
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	Reach = 150.0f;
	//Look for attached physics handle and input player handle
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	InputHandle = GetOwner()->FindComponentByClass<UInputComponent>();
	if (PhysicsHandle == nullptr || InputHandle==nullptr) {
		UE_LOG(LogTemp, Error, TEXT("Some handle is missing!"));
	}
	else {
		//Map methods for press and release event on input handle
		((UInputComponent*)InputHandle)->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		((UInputComponent*)InputHandle)->BindAction("Grab", IE_Released, this, &UGrabber::Release);
		((UInputComponent*)InputHandle)->BindAction("Quit", IE_Pressed, this, &UGrabber::Quit);
	}
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Check if there is grabed object
	if (PhysicsHandle->GrabbedComponent) {
		//Calculate position where grabed object will be drawn
		FRotator tempRot;
		FVector tempVec;
		GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(tempVec, tempRot);
		FVector TempLine = tempVec + (tempRot.Vector()*Reach);
		//Set grabed object to calculated vector
		PhysicsHandle->SetTargetLocation(TempLine);
	}
}

