// Milan Lazarevic

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Check if the mass of actors on PreassurePlate is enough heavy
	float currentMass = GetTotalMassOfActorsOnPlate();
	if (currentMass >= TriggerMass) {
		OnOpenRequest.Broadcast();
	}
	else {
		
		OnCloseRequest.Broadcast();
	}
}

//Calculates mass on Preasure Plate
float UOpenDoor::GetTotalMassOfActorsOnPlate() {
	float TotalMass = 0.0f;
	TArray<AActor*> overlapArr;
	//Get all actors on PreasurePlate
	PressurePlate->GetOverlappingActors(overlapArr);
	//Calculate mass for all actors on PreasurePlate
	for (const auto& Iterator: overlapArr) {
		UPrimitiveComponent *component=Iterator->FindComponentByClass<UPrimitiveComponent>();
		TotalMass+=((UPrimitiveComponent*)component)->GetMass();
	}
	return TotalMass;
}

