// Milan Lazarevic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/TriggerVolume.h"
#include "OpenDoor2.generated.h"

//Declares delegate used for broadcasting event for opening and closing second door
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent2);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ESCAPEGAME_API UOpenDoor2 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor2();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Method for calculating mass on PreassurePlate
	float GetTotalMassOfActorsOnPlate(ATriggerVolume* PressurePlate);

	//Open door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent2 OnOpenRequest;

	//Close door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent2 OnCloseRequest;

private:

	//Corner preassure plate 1
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate1;

	//Corner preassure plate 2
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate2;

	//Corner preassure plate 3
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate3;

	//Corner preassure plate 4
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate4;

	//Mass for preassure plate to react (mass of one chair)
	UPROPERTY(EditAnywhere)
		float TriggerMass = 20.0f;
	
};
