// Milan Lazarevic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/TriggerVolume.h"
#include "GameFramework/Actor.h"
#include "OpenDoor3.generated.h"

//Declares delegate used for broadcasting event for opening and closing red door
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent3);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEGAME_API UOpenDoor3 : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor3();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Open door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent3 OnOpenRequest;

	//Close door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent3 OnCloseRequest;

private:

	//Field near red door
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate;

	//Key used for red door opening
	UPROPERTY(EditAnywhere)
		AActor* key;

	//Current player
	UPROPERTY(EditAnywhere)
		AActor* player;
	
};
