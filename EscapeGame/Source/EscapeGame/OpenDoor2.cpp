// Milan Lazarevic

#include "OpenDoor2.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UOpenDoor2::UOpenDoor2()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor2::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UOpenDoor2::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	bool tempF = true;
	//Checks if all chairs are in one corner (preassure plate) through mass of object in preassure plate
	if (!(GetTotalMassOfActorsOnPlate(PressurePlate1) >= TriggerMass)) {
		tempF = false;
	}
	if (!(GetTotalMassOfActorsOnPlate(PressurePlate2) >= TriggerMass)) {
		tempF = false;
	}
	if (!(GetTotalMassOfActorsOnPlate(PressurePlate3) >= TriggerMass)) {
		tempF = false;
	}
	if (!(GetTotalMassOfActorsOnPlate(PressurePlate4) >= TriggerMass)) {
		tempF = false;
	}
	//if chairs are in corners triger open second door event
	if(tempF){
		OnOpenRequest.Broadcast();
	}
	else {
		
		OnCloseRequest.Broadcast();
	}

}

//Gets mass of actors on PreassurePlate
float UOpenDoor2::GetTotalMassOfActorsOnPlate(ATriggerVolume* PressurePlate) {
	float TotalMass = 0.0f;
	TArray<AActor*> overlapArr;
	//Get all actors on PreassurePlate
	PressurePlate->GetOverlappingActors(overlapArr);
	//Calculate mass of actors
	for (const auto& Iterator : overlapArr) {
		UPrimitiveComponent *component = Iterator->FindComponentByClass<UPrimitiveComponent>();
		TotalMass += ((UPrimitiveComponent*)component)->GetMass();
	}
	return TotalMass;
}

