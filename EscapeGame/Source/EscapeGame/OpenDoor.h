// Milan Lazarevic

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"

//Declares delegate used for broadcasting event for opening and closing first door
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEGAME_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	float GetTotalMassOfActorsOnPlate();

	//Open door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent OnOpenRequest;

	//Close door event
	UPROPERTY(BlueprintAssignable)
		FDoorEvent OnCloseRequest;

private:

	//Preasure plate in first room
	UPROPERTY(EditAnywhere)
		ATriggerVolume* PressurePlate;

	//Mass for activating first PreasurePlate (chair and table mass)
	UPROPERTY(EditAnywhere)
		float TriggerMass=50.0f;

};
