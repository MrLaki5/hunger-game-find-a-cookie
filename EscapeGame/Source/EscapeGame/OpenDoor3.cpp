// Milan Lazarevic

#include "OpenDoor3.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UOpenDoor3::UOpenDoor3()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UOpenDoor3::BeginPlay()
{
	Super::BeginPlay();
	//Gets current player
	player = GetWorld()->GetFirstPlayerController()->GetPawn();
}


// Called every frame
void UOpenDoor3::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//Check if key and player are near red door and if they are broadcasts red door opening
	if (PressurePlate->IsOverlappingActor(key) && PressurePlate->IsOverlappingActor(player)) {
		OnOpenRequest.Broadcast();
	}
	else {
		OnCloseRequest.Broadcast();
	}
}

